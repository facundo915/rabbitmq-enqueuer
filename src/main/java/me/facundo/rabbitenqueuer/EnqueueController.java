package me.facundo.rabbitenqueuer;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class EnqueueController {
    @Autowired
    RabbitTemplate rabbitTemplate;

    @PostMapping("/enqueue")
    public void enqueue(@RequestBody String url){
        rabbitTemplate.convertAndSend(RabbitenqueuerApplication.topicExchangeName, "foo.bar.baz", url);
    }
}
